import * as seqQueue from "seq-queue"
import * as Promise from "bluebird"
import * as net from "net"

import * as sonylibs from "./sonylibs"





export interface ISocketQueueConf {
  host: string
  port: number
}
export interface IBuff {
  bufType: string
  length: number
  buffCmd: Buffer
}

export interface IBuffForCoords {
  x?: IBuff
  y?: IBuff
  z?: IBuff
}



export interface ICamConf {
  model: { label: string, version?: string }
  host: string
  port: number
  capabilities: ICapabilities
  buffPositions?: IBuffForCoords
}

export interface IPololuConf {
  host: string
  port: number
}

export interface ICapabilities {
  clickToCenter?: boolean
  gotoHome?: boolean
  joystick: boolean
  move: boolean
  presets: boolean
  zoom: boolean
}


// Pass a value, get an array back.
// E.g. `6000` will be returned as `[01110000, 00101110]`
function toLowAndHighBits(value) {
  return [value & 0x7F, (value >> 7) & 0x7F];
}

// Pass an array, get an value back.
// E.g. `[01110000, 00101110]` will be returned as `6000`
function fromLowAndHighBits(data) {
  return ((data[1] << 7) + data[0]) & 0x7F7F;
}

// Pass an array, get an value back.
// E.g. `[01110000, 00010111]` will be returned as `6000`
function fromLowAndHigh8Bits(data) {
  return ((data[1] << 8) + data[0]) & 0xFFFF;
}


export class SocketQueue {

  socket: net.Socket
  
  queue: seqQueue.createQueue

  host: string
  port: number
  connected: boolean = false

  constructor(opts: ISocketQueueConf) {

    this.host = opts.host
    this.port = opts.port

    this.queue = seqQueue.createQueue(1000)


  }



  _newConnect() {
    const that = this
    return new Promise((resolve, reject) => {
      that.socket = new net.Socket()
      that.socket.connect(that.port, that.host, (err) => {
        if (err) return reject(err)
        that.connected = true;
        that.socket.on('close', () => {
          that._onClose()
        });
        that.socket.on('error', (err) => {
          that._onError(err)
        });
        resolve(true)
      });
    })

  }


  connect() {
    const that = this
    return new Promise((resolve, reject) => {
      if (that.connected) return resolve(true)
      if (that.socket) that.socket.destroy();
      that.connected = false;
      setTimeout(() => {
        that._newConnect().then(() => {
          resolve(true)
        }).catch((err) => {
          reject(err)
        })
      }, 500)

    })
  }


  disconnect() {
    if (this.socket) this.socket.destroy();
    this.connected = false;
  }

  _reconnect() {
    const that = this
    return new Promise((resolve, reject) => {
      if (that.socket) that.socket.destroy();
      that.connected = false;
      that._newConnect().then(() => {
        resolve(true)
      }).catch((err) => {
        reject(err)
      })
    })
  }

  _onError(err) {
    this.disconnect()
    return { error: err }
  }

  _onClose() {
    this.disconnect()
    return { status: 'closed' }
  }



  sendInteractiveCommand(buffConf: IBuff, noclose?: boolean) {
    const that = this
    return new Promise<any>((resolve, reject) => {
      this.queue.push((task) => {
        let buffer = Buffer.alloc(0)
        that.connect().then(() => {
          that.socket.on('data', (b) => {
            buffer = Buffer.concat([buffer, b])
            if (buffer.length === buffConf.length) {
              if (!noclose) that.disconnect()
              task.done()
              return resolve(buffer[buffConf.bufType](0))
            }
          })

          that.socket.write(buffConf.buffCmd, 'hex', (err) => {
            if (err) {
              if (!noclose) that.disconnect()
              task.done()
              return reject(that._onError(err))
            }
          });
        }).catch((err) => {
          if (!noclose) that.disconnect()
          task.done()
          return reject(err)
        })
      })
    })

  }
  sendCommand(buffCmd: Buffer, noclose?: boolean) {
    const that = this
    return new Promise<true>((resolve, reject) => {
      this.queue.push((task) => {
        that.connect().then(() => {
          that.socket.write(buffCmd, 'hex', (err) => {
            if (err) {
              if (!noclose) that.disconnect()
              task.done()
              return reject(that._onError(err))
            }
            setTimeout(() => {
              if (!noclose) that.disconnect()
              task.done()
              return resolve(true)
            }, 1000)
          });
        }).catch((err) => {
          if (!noclose) that.disconnect()
          task.done()
          return reject(err)
        })
      })
    })

  }

}


export class CamCmds extends SocketQueue {
  id: string
  model: { label: string, version?: string }
  capabilities: ICapabilities
  coords: [number, number, number] = [0, 0, 0]

  buffPositions: IBuffForCoords
  constructor(opts: ICamConf) {
    super({ host: opts.host, port: opts.port })
    this.model = opts.model
    this.capabilities = opts.capabilities
    this.buffPositions = opts.buffPositions
  }
  getCurrentCoords() {
    return this.coords
  }
  getCurrentPosition() {
    return { x: this.coords[0], y: this.coords[0], Z: this.coords[0] }
  }
  getCurrentPositionX() {
    return this.coords[0]
  }
  getCurrentPositionY() {
    return this.coords[1]
  }
  getCurrentPositionZ() {
    return this.coords[2]
  }


  _setCurrentCoords(coords: [number, number, number]) {
    return this.coords = coords
  }
  _setCurrentPosition(xyz: { x: number, y: number, z: number }) {
    return this.coords = [xyz.x, xyz.y, xyz.z]
  }
  _setCurrentPositionX(x: number) {
    return this.coords[0] = x
  }
  _setCurrentPositionY(y: number) {
    return this.coords[1] = y
  }
  _setCurrentPositionZ(z: number) {
    return this.coords[2] = z
  }

  getPos(coord: string) {
    const that = this
    return new Promise<number>((resolve, reject) => {
      that.sendInteractiveCommand(that.buffPositions[coord]).then((pos) => {
        that._setCurrentPositionX(pos)
        resolve(pos)
      })
    })
  }

  getX(noclose?: boolean) {
    const that = this
    return new Promise<number>((resolve, reject) => {
      that.sendInteractiveCommand(that.buffPositions.x, noclose).then((pos) => {
        that._setCurrentPositionX(pos)
        resolve(pos)
      })
    })
  }

  getY(noclose?: boolean) {
    const that = this
    return new Promise<number>((resolve, reject) => {
      that.sendInteractiveCommand(that.buffPositions.y, noclose).then((pos) => {
        that._setCurrentPositionX(pos)
        resolve(pos)
      })
    })
  }

  getZ(noclose?: boolean) {
    const that = this
    return new Promise<number>((resolve, reject) => {
      that.sendInteractiveCommand(that.buffPositions.z, noclose).then((pos) => {
        that._setCurrentPositionX(pos)
        resolve(pos)
      })
    })
  }

}



export class Pololu extends CamCmds {
  minRangeValue: number
  maxRangeValue: number
  movestep: number
  ZLevel: sonylibs.ZoomLevels

  constructor(opts: IPololuConf) {

    const xcmd: any = [0xAC, 0]
    const ycmd: any = [0xAC, 1]
    const zcmd: any = [0xAC, 2]




    super({
      model: { label: 'Pololu', version: '0.1.0' },
      host: opts.host,
      port: opts.port,
      capabilities: {
        move: true,
        gotoHome: false,
        clickToCenter: false,
        joystick: true,
        zoom: true,
        presets: false
      },
      buffPositions: {
        x: {
          length: 1,
          buffCmd: Buffer.from(xcmd, 'hex'),
          bufType: 'readUInt8'
        },
        y: {
          length: 1,
          buffCmd: Buffer.from(ycmd, 'hex'),
          bufType: 'readUInt8'
        },
        z: {
          length: 2,
          buffCmd: Buffer.from(zcmd, 'hex'),
          bufType: 'readUInt16BE'
        }
      }
    })

    this.movestep = 25
    this.minRangeValue = 0;
    this.maxRangeValue = 255;
    this.ZLevel = new sonylibs.ZoomLevels()

  }

  moveAllOfSteps(action: { coords: { x?: number, y?: number, z?: number }, noclose?: boolean }) {
    const that = this
    return new Promise<true>((resolve, reject) => {

      const arrb: any = []
      let e = 0
      if (action.coords.x) {
        arrb.push(0xAC)
        arrb.push(0)
        arrb.push(action.coords.x * this.movestep)
        e++
      }
      if (action.coords.y) {
        arrb.push(0xAC)
        arrb.push(1)
        arrb.push(action.coords.y * this.movestep)
        e++
      }
      if (action.coords.z) {
        arrb.push(0xAC)
        arrb.push(2)
        arrb.push(action.coords.z * this.movestep * 10)
        e++
      }

      that.sendCommand(Buffer.from(arrb, 'hex'), action.noclose).then(() => {

        resolve(true)

      }).catch((err) => {
        reject(err)
      })

    })
  }

  moveOfSteps(action: { motor: 0 | 1 | 3, steps: number, noclose?: boolean }) {
    switch (action.motor) {
      case 0:
        action.steps = action.steps * this.movestep
        break
      case 1:
        action.steps = action.steps * this.movestep
        break
      case 3:
        action.steps = action.steps * this.movestep * 10
        break
    }
    return this.moveOf({ motor: action.motor, val: action.steps * this.movestep, noclose: action.noclose })
  }

  moveMotorOfSteps(action: { motor: 0 | 1, steps: number, noclose?: boolean }) {
    return this.moveOf({ motor: action.motor, val: action.steps * this.movestep, noclose: action.noclose })
  }


  moveZoomOfSteps(action: { steps: number, noclose?: boolean }) {
    return this.moveOf({ motor: 2, val: action.steps * this.movestep, noclose: action.noclose })
  }


  moveOf(action: { motor: number, val: number, noclose?: boolean }) {
    const that = this
    return new Promise<true>((resolve, reject) => {
      const buffcmd: any = [0xAB, action.motor, action.val]

      that.sendCommand(Buffer.from(buffcmd, 'hex'), action.noclose).then(() => {
        resolve(true)
      }).catch((err) => {
        reject(err)
      })
    })
  }


  moveTo(action: { XVal: number, ZVal: number, noclose?: boolean }) {
    const that = this
    return new Promise<true>((resolve, reject) => {


      that.getX().then((X) => {
        that.getZ().then((Z) => {


          const SMove = new sonylibs.SonyMove()
          const absoluteXOfMotor = SMove.absolute_move(X, action.XVal, that.ZLevel.f(Z))
          const buffcmd: any = [0xAB, 0, absoluteXOfMotor]

          that.sendCommand(Buffer.from(buffcmd, 'hex'), true).then(() => {

            const SZoom = new sonylibs.SonyZoom()

            const visca_bytes: any = SZoom.set_zoom(Z, action.ZVal)

            that.sendCommand(Buffer.from(visca_bytes, 'hex'), action.noclose).then(() => {

              resolve(true)

            }).catch((err) => {
              reject(err)
            })


          }).catch((err) => {
            reject(err)
          })
        }).catch((err) => {
          reject(err)
        })
      }).catch((err) => {
        reject(err)
      })
    })
  }

}



