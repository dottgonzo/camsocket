import * as cameraUtil from "../index"
import * as chai from "chai"

const expect = chai.expect

const host = process.env.SOCKETHOST || 'localhost'
const port = parseInt(process.env.SOCKETPORT) || 32000


describe('PololuPosition', function () {
  this.timeout(60000)
  const Pololu = new cameraUtil.Pololu({ host: host, port: port })
  it('connect', (done) => {
    Pololu.connect().then(() => {
      expect(Pololu.connected).to.be.ok
      Pololu.disconnect()
      expect(!Pololu.connected).to.be.ok
      done()
    }).catch((err) => {
      done(new Error(err))
    })
  })
  it('get X', (done) => {
    Pololu.getX().then((X) => {
      console.log(X)
      expect(Pololu.connected).to.not.be.ok
      expect(X).to.be.ok
      expect(X).to.be.a('Number')
      done()
    }).catch((err) => {
      done(new Error(err))
    })
  })
  it('get Z', (done) => {
    Pololu.getZ().then((Z) => {
      console.log(Z)
      expect(Pololu.connected).to.not.be.ok
      expect(Z).to.be.a('Number')
      done()
    }).catch((err) => {
      done(new Error(err))
    })
  })
})





describe('PololuMoveTo', function () {
  this.timeout(60000)
  const Pololu = new cameraUtil.Pololu({ host: host, port: port })
  it('connect', (done) => {
    Pololu.connect().then(() => {
      expect(Pololu.connected).to.be.ok
      Pololu.disconnect()
      expect(!Pololu.connected).to.be.ok
      done()
    }).catch((err) => {
      done(new Error(err))
    })
  })
  it('move X Of 3', (done) => {
    Pololu.getX().then((oldX) => {

      Pololu.moveOf({ motor: 0, val: -80 }).then(() => {
        setTimeout(() => {
          Pololu.getX().then((newX) => {
            console.log('newX', newX)
            console.log('conn', Pololu.connected)

            expect(oldX - newX).to.be.ok
            done()
          }).catch((err) => {
            done(new Error(err))
          })
        }, 3000)
      }).catch((err) => {
        done(new Error(err))
      })
    }).catch((err) => {
      done(new Error(err))
    })
  })
  it('move To', (done) => {
    Pololu.getX().then((oldX) => {
      Pololu.getZ().then((oldZ) => {

        Pololu.moveTo({ XVal: -0.2119078104993598, ZVal: 3.3956521739130436 }).then((oldX) => {
          console.log('check')
          setTimeout(() => {
            Pololu.getX().then((newX) => {

              Pololu.getZ().then((newZ) => {
                expect(newZ - oldZ).to.be.not.eq(0)
                done()
              }).catch((err) => {
                done(new Error(err))
              })
            }).catch((err) => {
              done(new Error(err))
            })
          }, 3000)
        }).catch((err) => {
          done(new Error(err))
        })
      }).catch((err) => {
        done(new Error(err))
      })
    }).catch((err) => {
      done(new Error(err))
    })
  })
  it('zoom Out', (done) => {
    Pololu.getZ().then((oldZ) => {

      Pololu.moveOf({ motor: 2, val: 10 }).then((oldX) => {
        console.log('check')
        Pololu.getZ().then((newZ) => {
          expect(newZ - oldZ).to.be.not.eq(0)
          done()
        }).catch((err) => {
          done(new Error(err))
        })

      }).catch((err) => {
        done(new Error(err))
      })
    }).catch((err) => {
      done(new Error(err))
    })

  })
  it('zoom In', (done) => {
    Pololu.getZ().then((oldZ) => {

      Pololu.moveOf({ motor: 2, val: 1000 }).then((oldX) => {
        console.log('check')
        Pololu.getZ().then((newZ) => {
          expect(newZ - oldZ).to.be.not.eq(0)
          done()
        }).catch((err) => {
          done(new Error(err))
        })

      }).catch((err) => {
        done(new Error(err))
      })
    }).catch((err) => {
      done(new Error(err))
    })

  })
  it('zoom Med', (done) => {
    Pololu.getZ().then((oldZ) => {

      Pololu.moveOf({ motor: 2, val: 500 }).then((oldX) => {
        console.log('check')
        Pololu.getZ().then((newZ) => {
          expect(newZ - oldZ).to.be.not.eq(0)
          done()
        }).catch((err) => {
          done(new Error(err))
        })

      }).catch((err) => {
        done(new Error(err))
      })
    }).catch((err) => {
      done(new Error(err))
    })

  })
})



