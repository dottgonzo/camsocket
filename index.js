"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const seqQueue = require("seq-queue");
const Promise = require("bluebird");
const net = require("net");
const sonylibs = require("./sonylibs");
function toLowAndHighBits(value) {
    return [value & 0x7F, (value >> 7) & 0x7F];
}
function fromLowAndHighBits(data) {
    return ((data[1] << 7) + data[0]) & 0x7F7F;
}
function fromLowAndHigh8Bits(data) {
    return ((data[1] << 8) + data[0]) & 0xFFFF;
}
class SocketQueue {
    constructor(opts) {
        this.connected = false;
        this.host = opts.host;
        this.port = opts.port;
        this.queue = seqQueue.createQueue(1000);
    }
    _newConnect() {
        const that = this;
        return new Promise((resolve, reject) => {
            that.socket = new net.Socket();
            that.socket.connect(that.port, that.host, (err) => {
                if (err)
                    return reject(err);
                that.connected = true;
                that.socket.on('close', () => {
                    that._onClose();
                });
                that.socket.on('error', (err) => {
                    that._onError(err);
                });
                resolve(true);
            });
        });
    }
    connect() {
        const that = this;
        return new Promise((resolve, reject) => {
            if (that.connected)
                return resolve(true);
            if (that.socket)
                that.socket.destroy();
            that.connected = false;
            setTimeout(() => {
                that._newConnect().then(() => {
                    resolve(true);
                }).catch((err) => {
                    reject(err);
                });
            }, 500);
        });
    }
    disconnect() {
        if (this.socket)
            this.socket.destroy();
        this.connected = false;
    }
    _reconnect() {
        const that = this;
        return new Promise((resolve, reject) => {
            if (that.socket)
                that.socket.destroy();
            that.connected = false;
            that._newConnect().then(() => {
                resolve(true);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    _onError(err) {
        this.disconnect();
        return { error: err };
    }
    _onClose() {
        this.disconnect();
        return { status: 'closed' };
    }
    sendInteractiveCommand(buffConf, noclose) {
        const that = this;
        return new Promise((resolve, reject) => {
            this.queue.push((task) => {
                let buffer = Buffer.alloc(0);
                that.connect().then(() => {
                    that.socket.on('data', (b) => {
                        buffer = Buffer.concat([buffer, b]);
                        if (buffer.length === buffConf.length) {
                            if (!noclose)
                                that.disconnect();
                            task.done();
                            return resolve(buffer[buffConf.bufType](0));
                        }
                    });
                    that.socket.write(buffConf.buffCmd, 'hex', (err) => {
                        if (err) {
                            if (!noclose)
                                that.disconnect();
                            task.done();
                            return reject(that._onError(err));
                        }
                    });
                }).catch((err) => {
                    if (!noclose)
                        that.disconnect();
                    task.done();
                    return reject(err);
                });
            });
        });
    }
    sendCommand(buffCmd, noclose) {
        const that = this;
        return new Promise((resolve, reject) => {
            this.queue.push((task) => {
                that.connect().then(() => {
                    that.socket.write(buffCmd, 'hex', (err) => {
                        if (err) {
                            if (!noclose)
                                that.disconnect();
                            task.done();
                            return reject(that._onError(err));
                        }
                        setTimeout(() => {
                            if (!noclose)
                                that.disconnect();
                            task.done();
                            return resolve(true);
                        }, 1000);
                    });
                }).catch((err) => {
                    if (!noclose)
                        that.disconnect();
                    task.done();
                    return reject(err);
                });
            });
        });
    }
}
exports.SocketQueue = SocketQueue;
class CamCmds extends SocketQueue {
    constructor(opts) {
        super({ host: opts.host, port: opts.port });
        this.coords = [0, 0, 0];
        this.model = opts.model;
        this.capabilities = opts.capabilities;
        this.buffPositions = opts.buffPositions;
    }
    getCurrentCoords() {
        return this.coords;
    }
    getCurrentPosition() {
        return { x: this.coords[0], y: this.coords[0], Z: this.coords[0] };
    }
    getCurrentPositionX() {
        return this.coords[0];
    }
    getCurrentPositionY() {
        return this.coords[1];
    }
    getCurrentPositionZ() {
        return this.coords[2];
    }
    _setCurrentCoords(coords) {
        return this.coords = coords;
    }
    _setCurrentPosition(xyz) {
        return this.coords = [xyz.x, xyz.y, xyz.z];
    }
    _setCurrentPositionX(x) {
        return this.coords[0] = x;
    }
    _setCurrentPositionY(y) {
        return this.coords[1] = y;
    }
    _setCurrentPositionZ(z) {
        return this.coords[2] = z;
    }
    getPos(coord) {
        const that = this;
        return new Promise((resolve, reject) => {
            that.sendInteractiveCommand(that.buffPositions[coord]).then((pos) => {
                that._setCurrentPositionX(pos);
                resolve(pos);
            });
        });
    }
    getX(noclose) {
        const that = this;
        return new Promise((resolve, reject) => {
            that.sendInteractiveCommand(that.buffPositions.x, noclose).then((pos) => {
                that._setCurrentPositionX(pos);
                resolve(pos);
            });
        });
    }
    getY(noclose) {
        const that = this;
        return new Promise((resolve, reject) => {
            that.sendInteractiveCommand(that.buffPositions.y, noclose).then((pos) => {
                that._setCurrentPositionX(pos);
                resolve(pos);
            });
        });
    }
    getZ(noclose) {
        const that = this;
        return new Promise((resolve, reject) => {
            that.sendInteractiveCommand(that.buffPositions.z, noclose).then((pos) => {
                that._setCurrentPositionX(pos);
                resolve(pos);
            });
        });
    }
}
exports.CamCmds = CamCmds;
class Pololu extends CamCmds {
    constructor(opts) {
        const xcmd = [0xAC, 0];
        const ycmd = [0xAC, 1];
        const zcmd = [0xAC, 2];
        super({
            model: { label: 'Pololu', version: '0.1.0' },
            host: opts.host,
            port: opts.port,
            capabilities: {
                move: true,
                gotoHome: false,
                clickToCenter: false,
                joystick: true,
                zoom: true,
                presets: false
            },
            buffPositions: {
                x: {
                    length: 1,
                    buffCmd: Buffer.from(xcmd, 'hex'),
                    bufType: 'readUInt8'
                },
                y: {
                    length: 1,
                    buffCmd: Buffer.from(ycmd, 'hex'),
                    bufType: 'readUInt8'
                },
                z: {
                    length: 2,
                    buffCmd: Buffer.from(zcmd, 'hex'),
                    bufType: 'readUInt16BE'
                }
            }
        });
        this.movestep = 25;
        this.minRangeValue = 0;
        this.maxRangeValue = 255;
        this.ZLevel = new sonylibs.ZoomLevels();
    }
    moveAllOfSteps(action) {
        const that = this;
        return new Promise((resolve, reject) => {
            const arrb = [];
            let e = 0;
            if (action.coords.x) {
                arrb.push(0xAC);
                arrb.push(0);
                arrb.push(action.coords.x * this.movestep);
                e++;
            }
            if (action.coords.y) {
                arrb.push(0xAC);
                arrb.push(1);
                arrb.push(action.coords.y * this.movestep);
                e++;
            }
            if (action.coords.z) {
                arrb.push(0xAC);
                arrb.push(2);
                arrb.push(action.coords.z * this.movestep * 10);
                e++;
            }
            that.sendCommand(Buffer.from(arrb, 'hex'), action.noclose).then(() => {
                resolve(true);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    moveOfSteps(action) {
        switch (action.motor) {
            case 0:
                action.steps = action.steps * this.movestep;
                break;
            case 1:
                action.steps = action.steps * this.movestep;
                break;
            case 3:
                action.steps = action.steps * this.movestep * 10;
                break;
        }
        return this.moveOf({ motor: action.motor, val: action.steps * this.movestep, noclose: action.noclose });
    }
    moveMotorOfSteps(action) {
        return this.moveOf({ motor: action.motor, val: action.steps * this.movestep, noclose: action.noclose });
    }
    moveZoomOfSteps(action) {
        return this.moveOf({ motor: 2, val: action.steps * this.movestep, noclose: action.noclose });
    }
    moveOf(action) {
        const that = this;
        return new Promise((resolve, reject) => {
            const buffcmd = [0xAB, action.motor, action.val];
            that.sendCommand(Buffer.from(buffcmd, 'hex'), action.noclose).then(() => {
                resolve(true);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    moveTo(action) {
        const that = this;
        return new Promise((resolve, reject) => {
            that.getX().then((X) => {
                that.getZ().then((Z) => {
                    const SMove = new sonylibs.SonyMove();
                    const absoluteXOfMotor = SMove.absolute_move(X, action.XVal, that.ZLevel.f(Z));
                    const buffcmd = [0xAB, 0, absoluteXOfMotor];
                    that.sendCommand(Buffer.from(buffcmd, 'hex'), true).then(() => {
                        const SZoom = new sonylibs.SonyZoom();
                        const visca_bytes = SZoom.set_zoom(Z, action.ZVal);
                        that.sendCommand(Buffer.from(visca_bytes, 'hex'), action.noclose).then(() => {
                            resolve(true);
                        }).catch((err) => {
                            reject(err);
                        });
                    }).catch((err) => {
                        reject(err);
                    });
                }).catch((err) => {
                    reject(err);
                });
            }).catch((err) => {
                reject(err);
            });
        });
    }
}
exports.Pololu = Pololu;
//# sourceMappingURL=index.js.map