FROM node:8-alpine
WORKDIR /app
COPY ./package.json /app/package.json
RUN npm i --production
COPY ./index.js /app/index.js
COPY ./andreLib.js /app/andreLib.js
COPY ./router.js /app/router.js
COPY ./server.js /app/server.js
CMD npm run server